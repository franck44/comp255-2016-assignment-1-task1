
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Test suite for Longest Common Subsequences
 */
public class ProcessManagerTests  {

    /**
     * Test for spawning a process
     */
    @Test
    public void templateTest() throws Exception {
        //  program to spawn and arguments
        String prog = "/Users/franck/development/COMP255-2016/assignment-1-submissions/task-1/sleepAndEcho.sh";
        //  10 seconds
        String[] args = { "10", "20" };

        //  create a process manager to interact with prog
        try {
        ProcessManager p = new ProcessManager(prog, args);
        p.spawn();
        TimeUnit.SECONDS.sleep(5);
        p.destroy();
    }
        catch (Exception e) {
            throw(e);
        }
    }

}
